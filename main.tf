# Docs on AWS provider configuration https://registry.terraform.io/providers/hashicorp/aws/latest/docs#example-usage
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.0"
    }
  }
}

provider "aws" {
  region  = "us-east-1"
}

resource "aws_key_pair" "key_name" {
  key_name = "Your_key_name"
  public_key = "Your_public_Key"  
}

resource "aws_instance" "instance_name" {
  ami = "ami-042e8287309f5df03"
  instance_type = "t2.micro"
  key_name = "Your_public_key"
  vpc_security_group_ids = [aws_security_group.allow-acess.id]

  tags = {
    name = "tag_name"
  }
}
