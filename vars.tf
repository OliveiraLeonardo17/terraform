variable "amis" {
  type = map

  description = "AMI (Amazon Machine Image) to use for the instance."

  default = {
    "us-east-1" = "ami-026c8acd92718196b" 
  }
}

variable "cdirs_remote_access" {
  type = list

  description = "List of CIDR blocks."

  default = ["0.0.0.0/0"]
}

variable "key_name" {
  type = string
  
  description = "Access Key name."
  
  default = "terraform"
}
